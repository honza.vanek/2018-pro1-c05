package students;

public class Time {
    private final int hour, minute;

    public Time(final int hour, final int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return minute;
    }

    public int minutesOfDay() {
        return this.hour * 60 + this.minute;
    }
}
