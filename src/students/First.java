package students;

import java.util.*;

class First {
	private static Random random = new Random();
	private static Scanner input = new Scanner(System.in);


	//A: Vypis studenta
	//I: Promenne s udaji o studentovi
	//O: Vypis v konzoli = side effect => navratova hodnota void
	
		   //O: 	      //A:		//I:
	static void/*FUJ*/ printStudent(final Student student) {
		final Time time = student.getTime();
		System.out.println("Jmeno: " + student.getFirstName() +
				"\nPrijmeni: " + student.getLastName() +
				"\nCas: " + time.getHour() + ":" + time.getMinute()
		);
	}

	static Student nextStudent(final String whichStudent) {
		System.out.println("Zadej udaje " + whichStudent + " studenta");
		System.out.println("Zadej krestni jmeno:");
		String firstName = input.next();
		System.out.println("Zadej prijmeni:");
		String lastName = input.next();
		System.out.println("Zadej hodinu:");
		int hour = input.nextInt();
		System.out.println("Zadej minutu:");
		int minute = input.nextInt();
		return new Student(firstName, lastName, new Time(hour, minute));
	}

	public static void main(String[] args) {
		//A: Nacist jmeno, prijmeni, hodinu, minutu dvou studetnu
		//I: Udaje z konzole
		//O: Promenne s udaji o studentech

		final Classroom classroomEmpty;
		if (random.nextInt(10) > 5)
			classroomEmpty = new ClassroomArray();
		else
			classroomEmpty = new ClassroomLinked();

		final Classroom classroom =
				classroomEmpty
				.add(nextStudent("prvniho"))
				.add(nextStudent("druheho"))
				.add(nextStudent("tretiho"));

		try {
			printStudent(classroom.get(0));
			printStudent(classroom.get(1));
			printStudent(classroom.get(2));
		} catch (InvalidIndexException e) {
			e.printStackTrace();
//			return;
//			throw ...
		} finally {
			System.out.println("finally");
		}
		//A: Ucit udaje studenta, ktery odevzdal jako prvni
		//I: Promenne s udaji o studentech (z predchoziho algoritum)
		//O: Promenne s udaji o studentovi

		final Student student = classroom.getFastestStudent();

		System.out.println("===== prvni odevzdal =====");
		//A: Vypis studenta
		//I: Promenne s udaji o studentovi
		//O: Vypis v konzoli
		printStudent(student);
	}
}
