package students;

public class Student {
    private final String firstName, lastName;
    private final Time time;

    public Student(final String firstName, final String lastName,
            final Time time) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.time = time;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Time getTime() {
        return time;
    }
}
