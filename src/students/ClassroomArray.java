package students;

import java.util.Objects;

public class ClassroomArray {
    private final Student[] students;

    public ClassroomArray() {
        students = new Student[0];
    }

    private ClassroomArray(final Student[] students) {
        this.students = students;
    }

    public ClassroomArray add(final Student student) {
        Objects.requireNonNull(student);
        final Student[] newStudents = new Student[students.length + 1];
        for (int i = 0; i < students.length; i++)
            newStudents[i] = students[i];
        newStudents[newStudents.length - 1] = student;
        return new ClassroomArray(newStudents);
    }

    /**
     * Returns an element at index i
     * @param i index of the element to be returned
     * @return student at the given index
     */

    public Student get(final int i) throws InvalidIndexException {
        if (i >=0 && i < students.length)
            return students[i];
        throw new InvalidIndexException();
    }


    private Student getFastest(final Student currentFastest,
                               final int i) {
        if (i == students.length)
            return currentFastest;
        return getFastest(
            //rychlejsi z currentFastest a students[i]
            currentFastest.getTime().minutesOfDay()
                < students[i].getTime().minutesOfDay()
                ? currentFastest : students[i]
            ,
            i + 1
        );
    }

    public Student getFastestStudent() {
        return getFastest(students[0], 1);
    }

//    public Student getFastestStudent() {
//        Student fastest = students[0];
//        for (int i = 1; i < students.length; i++)
//            if (fastest.getTime().minutesOfDay()
//                    > students[i].getTime().minutesOfDay())
//                fastest = students[i];
//        return fastest;
//    }

}
