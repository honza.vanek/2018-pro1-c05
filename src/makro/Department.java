package makro;

public class Department {
    private final String name;
    private final double taxRate;

    public Department(String name, double taxRate) {
        this.name = name;
        this.taxRate = taxRate;
    }

    public String getName() {
        return name;
    }

    public double getTaxRate() {
        return taxRate;
    }
}
