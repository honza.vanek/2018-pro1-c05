package makro;

public class Item {
    private final double amount;
    private final Discount discount;
    private final Product product;

    public Item(double amount, Discount discount, Product product) {
        this.amount = amount;
        this.discount = discount;
        this.product = product;
    }

    public double getAmount() {
        return amount;
    }

    public Discount getDiscount() {
        return discount;
    }

    public Product getProduct() {
        return product;
    }

    public double finalPrice() {
        return amount
                * product.priceIncludingTax()
                * (1 - discount.getValue());
    }
}
