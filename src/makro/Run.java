package makro;

import java.util.*;

class Run {
    private static Random random = new Random();
    private static Scanner input = new Scanner(System.in);

    //DU: vyrobit OrderArray a OrderLinked

    public static void main(String[] args) {
        final Department
                groceries = new Department("groceries", 0.15),
                toiletries = new Department("toiletries", 0.21);
        final Discount
                noDiscount = new Discount("none", 0),
                summerSale = new Discount("summer sale", 0.1);
        final Product
                bun = new Product(
                groceries,"bun", "pc", 2.9),
                soap = new Product(
                        toiletries,"soap", "l",59.9);
        final Order order = new Order(
                new Item(5, noDiscount, bun),
                new Item(1.5, summerSale, soap)
        );
        System.out.println(order.totalPrice());
    }
}
