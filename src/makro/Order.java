package makro;

public class Order {
    private final Item item1, item2;
    public Order(Item item1, Item item2) {
        this.item1 = item1;
        this.item2 = item2;
    }

    public double totalPrice() {
        return item1.finalPrice() + item2.finalPrice();
    }
}
