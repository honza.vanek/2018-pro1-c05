package makro;

public class Product {
    private final Department department;
    private final String label, unit;
    private final double unitPrice;

    public Product(Department department, String label, String unit, double unitPrice) {
        this.department = department;
        this.label = label;
        this.unit = unit;
        this.unitPrice = unitPrice;
    }

    public Department getDepartment() {
        return department;
    }

    public String getLabel() {
        return label;
    }

    public String getUnit() {
        return unit;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public double priceIncludingTax() {
        return unitPrice * (1 + department.getTaxRate());
    }
}
